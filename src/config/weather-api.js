import request from '@/util/weather-request'

// 获取 高德地图 天气
export function getAmapWeather() {
    return request({
        // url: 'https://restapi.amap.com/v3/weather/weatherInfo?key=161d326fcc4a717a0763eba4feb06551&city=巴林右旗',
        url: 'https://restapi.amap.com/v3/weather/weatherInfo?key=b084e619986d42acfc85be374da6d050&city=巴林右旗',
        method: 'get',
    })
}