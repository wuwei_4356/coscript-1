module.exports = {
  // publicPath: process.env.NODE_ENV === 'production'? '/' : '/',         // 域名子路由
  publicPath: './',

  // outputDir: __dirname + '/../server/public', // 构建输出目录,
  // 静态资源目录（js, css, img, fonts）
  assetsDir: 'assets',

  // 是否开启 eslint 保存验证 
  lintOnSave: false,

  configureWebpack: {
    resolve: {
      // 别名
      alias: {
        // '@': 'src',
        'assets': '@/assets',
        'api': '@/api',
        'utils': '@/utils',
        'components': '@/components',
        'common': '@/components/common',
        'views': '@/views',
      }
    },
  },
  
  devServer: {
    open: true,
    host: '0.0.0.0',
    port: '8088',
    https: false,
    hotOnly: false,

    // 配置跨域
    proxy: {
      '/api': {
        target: 'https://dsj.dayaar.com.cn',
        ws: true,
        changeOrigin: true,
        pathRewrite: {
          '^/api': ''
        }
      }
    },
  }
}
