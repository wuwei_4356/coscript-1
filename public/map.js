//初始化设置页面
function sel_menu(id) {
    type = id
    $(".menu a").each(function(i, elem) {
        $(elem).removeClass("active");
    });
    $("#menu_" + id).addClass("active");



    SetMap(lockplace);
    $(".container_right").animate({ opacity: '0.0' }, 100);
    //$(".container_right").show(1000)
}

function SetMap(name) {
    var opts = {
        subdistrict: 0, //返回下一级行政区
        extensions: "all", //返回行政区边界坐标组等具体信息
        showbiz: false,
        level: "city"
    };
    var district = new AMap.DistrictSearch(opts);

    district.search(name, function(status, result) {
        var bounds = result.districtList[0].boundaries;
        centerPoint = result.districtList[0].center
        var mask = []
        for (var i = 0; i < bounds.length; i += 1) {
            mask.push([bounds[i]])
        }
        
        map = new AMap.Map('container', {
            mask: mask,
            viewMode: '3D',
            rotateEnable: true,
            skyColor: "transparent",
            pitch: 50,
            zoom: 10,
            terrain: true,
            rotate: 0,
            animateEnable: true,
            center: centerPoint,
            layers: [
                new AMap.TileLayer.Satellite(),
                //new AMap.TileLayer.RoadNet()
            ]
        });

        //添加高度面
        var object3Dlayer = new AMap.Object3DLayer({
            zIndex: 9
        });
        map.add(object3Dlayer)
        var height = -90000;
        var color = '#326791'; //rgba
        var wall = new AMap.Object3D.Wall({
            path: bounds,
            height: height,
            color: color
        });
        wall.transparent = true
        object3Dlayer.add(wall)

            //添加描边
        for (var i = 0; i < bounds.length; i += 1) {
            new AMap.Polyline({
                path: bounds[i],
                strokeColor: '#99ffff',
                strokeWeight: 4,
                map: map
            })
        }

        //获取天气
        GetWeather(name)


        //划线行政区划
        initPro(result.districtList[0].adcode, 2);


        map.on('click', function(ev) {
            mapclick(ev)
        });

    });

}

function initPro(code, dep) {
    dep = typeof dep == 'undefined' ? 2 : dep;
    adCode = code;
    depth = dep;
    disProvince && disProvince.setMap(null);
    disProvince = new AMap.DistrictLayer.Province({
        zIndex: 12,
        adcode: [code],
        depth: dep,
        styles: {
            'fill': function(properties) {
                return areacolor(properties.NAME_CHN);
            },
            'province-stroke': 'cornflowerblue',
            'city-stroke': 'rgba(246,242,211,1)', // 中国地级市边界
            'county-stroke': 'rgba(246,242,211,1)' // 中国区县边界
        }

    });

    disProvince.setMap(map);
    getAreaAjax(lockplace)
}


//获取地址
function getAreaAjax(area_id) {

    $.ajax({
        type: "post",
        url: "map.php?t=area2",
        dataType: "json",
        data: {
            area_id: area_id,
            type: type,
        },
        success: function(a) {
            if (typeof a != "undefined" && a != "") {

                $("#load").fadeOut(1000);
                list = a;
                //处理数据
                if (type == 1) {
                    $("#p_total").show()
                    $("#p_yz").hide()
                    $("#p_cy").hide()
                } else if (type == 2) {
                    $("#p_total").hide()
                    $("#p_yz").show()
                    $("#p_cy").hide()
                } else if (type == 3) {
                    $("#p_total").hide()
                    $("#p_yz").hide()
                    $("#p_cy").show()
                }
                $(".container_right").animate({ opacity: '1.0' }, 200, function() {

                    setData(list);
                });
                makerAgps(list.area_next) //旗县标注
                map.setFitView();

            }
        }
    });
}

function areacolor(obj) {
    var color = 'rgba(237,120,26,0.2)';
    return color;
}



function mapclick(ev) {
    var px = ev.pixel;
    // 拾取所在位置的行政区
    var props = disProvince.getDistrictByContainerPos(px);
    if (props) {
        var NAME_CHN = props.NAME_CHN;
        if (NAME_CHN) {
            // 重置行政区样式
            disProvince.setStyles({
                'fill': function(props) {
                    return props.NAME_CHN == NAME_CHN ? 'rgba(237,120,26,0.2)' : 'rgba(0, 0, 0, 0.5)';
                },
                'province-stroke': 'cornflowerblue',
                'city-stroke': 'rgba(246,242,211,1)', // 中国地级市边界
                'county-stroke': 'rgba(246,242,211,0)' // 中国区县边界
            });
            $("#back").fadeIn(500)
            getAreaAjax(NAME_CHN, true);
            //updateInfo(props);
        }

    }
}


function rotate() {
    window.requestAnimationFrame(() => {
        //if (mapAnimateControl == true) {
        map.setRotation((map.getRotation() + 0.02) % 360);
        // }
        rotate()
    })
}


function setData(obj) {



    $("#cattle_totle_yz_num").html(obj.area_num.cattle_totle_yz_num.replace(/\B(?=(?:\d{3})+(?!\d))/g, ','));
    $("#cattle_totle_my_num").html(obj.area_num.cattle_totle_my_num.replace(/\B(?=(?:\d{3})+(?!\d))/g, ','));
    $("#cattle_totle_jb_num").html(obj.area_num.cattle_totle_jb_num.replace(/\B(?=(?:\d{3})+(?!\d))/g, ','));

    $("#cattle_totle_jcmu_num").html(obj.area_num.cattle_totle_jcmu_num.replace(/\B(?=(?:\d{3})+(?!\d))/g, ','));
    $("#cattle_totle_gn_num").html(obj.area_num.cattle_totle_gn_num.replace(/\B(?=(?:\d{3})+(?!\d))/g, ','));
    $("#cattle_totle_rsq_num").html(obj.area_num.cattle_totle_rsq_num.replace(/\B(?=(?:\d{3})+(?!\d))/g, ','));
    $("#cattle_totle_dn_num").html(obj.area_num.cattle_totle_dn_num.replace(/\B(?=(?:\d{3})+(?!\d))/g, ','));



    if (type == 1) {
        $("#p_total").show()
        $("#p_yz").hide()
        $("#p_cy").hide()
        sel_chart_totle('', 'user');
        sel_chart_user('', 'age');
        sel_chart_market('', 'c');
        $("#cattle_totle_num").html(obj.area_num.cattle_totle_num.replace(/\B(?=(?:\d{3})+(?!\d))/g, ','));
        $("#ranch_totle_num").html(obj.area_num.ranch_totle_num.replace(/\B(?=(?:\d{3})+(?!\d))/g, ','));
        $("#totle_viston").html(obj.area_num.totle_viston.replace(/\B(?=(?:\d{3})+(?!\d))/g, ','));
        $("#member_totle").html(obj.area_num.member_totle.replace(/\B(?=(?:\d{3})+(?!\d))/g, ','));
        if (obj.area_cart.markt_c_total) {
            $("#market_c_price").html(obj.area_cart.markt_c_total[obj.area_cart.markt_c_total.length - 1].replace(/\B(?=(?:\d{3})+(?!\d))/g, ','));
        }
        if (obj.area_cart.markt_d_total) {
            $("#market_d_price").html(obj.area_cart.markt_d_total[obj.area_cart.markt_d_total.length - 1].replace(/\B(?=(?:\d{3})+(?!\d))/g, ','));
        }
        $("#market_sales_num").html(obj.area_num.market_sales_num.replace(/\B(?=(?:\d{3})+(?!\d))/g, ','));

    } else if (type == 2) {
        $("#p_total").hide()
        $("#p_yz").show()
        $("#p_cy").hide()
        $("#yz_totle_num").html(obj.area_num.cattle_totle_yz_num.replace(/\B(?=(?:\d{3})+(?!\d))/g, ','));
        // $("#yz_cglv").html(obj.area_num.yc_cgl + "%");
        $("#yz_cglv").html(obj.area_num.cattle_totle_yz_herdsNum.replace(/\B(?=(?:\d{3})+(?!\d))/g, ','));
        $("#yz_yzy").html(obj.area_num.yz_yzy.replace(/\B(?=(?:\d{3})+(?!\d))/g, ','));
        sel_breeding_totle()
        sel_pz_totle()
        sel_born_totle()

    } else if (type == 3) {
        $("#p_total").hide()
        $("#p_yz").hide()
        $("#p_cy").show()

        $("#yz_cy_total").html(obj.area_num.ranch_totle_num.replace(/\B(?=(?:\d{3})+(?!\d))/g, ','));


        if (loca) {
            loca.setMap(null);
        }

        loca = new Loca.PointLayer({
            map: map
        });
        loca.setData(list.area_point, {
            lnglat: "gps"
        });
        loca.setOptions({
            style: {
                // 圆形半径，单位像素
                radius: 1,
                // 填充颜色
                color: '#7bf9f7',
                // 描边颜色
                borderColor: '#23fafe',
                // 描边宽度，单位像素
                borderWidth: 1,
                // 透明度 [0-1]
                opacity: 0.9,
            }
        });

        loca.render();
        sel_cy_totle()
        sel_gm_totle()
        sel_top_totle()
        $("img").error(function() {
            $(this).attr('src', 'static/img/user.jpg');
        });


    }


}

function back() {
    map.clearMap();
    $("#back").fadeOut(500);
    // initPro(adcode, 2);
    disProvince.setStyles({
        'fill': function(properties) {
            return areacolor(properties.NAME_CHN);
        },
        'province-stroke': 'cornflowerblue',
        'city-stroke': 'rgba(246,242,211,1)', // 中国地级市边界
        'county-stroke': 'rgba(246,242,211,1)'
    });
    getAreaAjax(lockplace)
}





//海量点
function GetProInfo(type) {
    $("#protype").val(type);

    var colors = ["#74cfa6", "#74cfa6", "#ae7aee", "#f2661a", "#ffe700"];
    var area_id = $(".m_list").attr("date-area-id");
    var year = cutyear;

    AMapUI.load(["ui/misc/PointSimplifier", "lib/$"], function(
        PointSimplifier,
        $
    ) {
        if (!PointSimplifier.supportCanvas) {
            alert("当前环境不支持 Canvas！");
            return;
        }
        if (pointSimplifierIns) {
            pointSimplifierIns.hide();
        }
        pointSimplifierIns = new PointSimplifier({
            map: map, //所属的地图实例
            autoSetFitView: false,
            getPosition: function(item) {
                if (!item) {
                    return null;
                }
                var parts = item.pro_gps.split(",");
                //返回经纬度
                return [parseFloat(parts[0]), parseFloat(parts[1])];
            },
            getHoverTitle: function(dataItem, idx) {
                return "<b>" + dataItem.pro_name + "</b><br/>" + dataItem.pro_area;
            },
            renderConstructor: PointSimplifier.Render.Canvas.GroupStyleRender,
            renderOptions: {
                //点的样式
                pointStyle: {
                    width: 2,
                    height: 2
                },
                getGroupId: function(item, idx) {
                    //var parts = item.split(',');
                    return item.pro_type;
                },
                groupStyleOptions: function(gid) {
                    return {
                        pointStyle: {
                            //content: gid % 2 ? 'circle' : 'rect',
                            fillStyle: colors[gid],
                            width: 2,
                            height: 2
                        }
                    };
                },
                //鼠标hover时的title信息
                hoverTitleStyle: {
                    position: "top"
                }
            }
        });

        window.pointSimplifierIns = pointSimplifierIns;

        $.ajax({
            type: "post",
            url: "map.php?t=point",
            dataType: "json",
            data: {
                area_id: area_id,
                protype: type,
                proyear: year
            },
            success: function(a) {
                if (typeof a != "undefined" && a != "") {
                    pointSimplifierIns.setData(a);
                }
            }
        });

        pointSimplifierIns.on("pointClick", function(e, record) {
            // e.stopPropagation();
            pro_onClick(record.data);
            return false;
        });
    });
}

//标注
function makerAgps(list) {
    map.clearMap();
    if (typeof list !== "undefined") {
        for (i = 0; i < list.length; i++) {
            var div = document.createElement("div");
            div.className = "circle_area";
            Q = list[i].num.replace(/\B(?=(?:\d{3})+(?!\d))/g, ',')
            if (type == 1) {
                $D = '头'
            } else if (type == 2) {
                $D = '次'
            } else if (type == 3) {
                $D = '户'
            }
            div.innerHTML =
                "<em></em><span id='hotpoint_" +
                list[i].id +
                "' class='hotpoint'></span><b>" +
                list[i].area_name + ': <a>' + Q + ' </a>' + $D + ''
            "</b>";
            var marker = new AMap.Marker({
                extData: list[i].edatea,
                content: div,
                animation: "AMAP_ANIMATION_DROP",
                position: list[i].gps.split(","),
                map: map,
                offset: new AMap.Pixel(-40, -40)
            });
            // marker.on("click", markerClick);
        }
    }
}


function GetWeather(name) {
    AMap.plugin("AMap.Weather", function() {
        var weather = new AMap.Weather();
        weather.getLive(name, function(err, data) {
            if (!err) {
                var html =
                    "" +
                    "" +
                    data.weather +
                    "<br/>" +
                    "" +
                    data.windDirection +
                    "风" +
                    "<b>" +
                    data.temperature +
                    "℃ </b>";

                $("#Weather").html(html);
            }
        });
    });
}

AMap.plugin("AMap.Weather", function() {
    var weather = new AMap.Weather();
    weather.getLive(name, function(err, data) {
        if (!err) {
            var html =
                "" +
                "" +
                data.weather +
                "<br/>" +
                "" +
                data.windDirection +
                "风" +
                "<b>" +
                data.temperature +
                "℃ </b>";

            $("#Weather").html(html);
        }
    });
});


function getDate() {
    var today = new Date();
    var date =
        twoDigits(today.getMonth() + 1) + "/" + twoDigits(today.getDate()) + "";
    var week = " 星期" + "日一二三四五六 ".charAt(today.getDay());
    var time = twoDigits(today.getHours()) + ":" + twoDigits(today.getMinutes());
    $(".time").html(time);
}

function twoDigits(val) {
    if (val < 10) return "0" + val;
    return val;
}

$(function() {
    setInterval(getDate, 1000);
});

function settab(tab) {
    $(".header-bar a").each(function(i, elem) {
        //一参:下标
        $(elem).removeClass("active");

        if (tab == i) {
            $(elem).addClass("active");
        }
    });
    type = tab;
    getAreaAjax(); //初始化
}











































function sel_chart_totle(obj, type) {
	console.log(list);
    $(".tag_total a").each(function(i, elem) {
        $(elem).removeClass("active");
        if (!obj && i == 0) {
            $(elem).addClass("active");
        }
    });

    if (obj) {
        $(obj).addClass("active");
    }

    option = {
        grid: { //柱状图位置
            top: "10%",
            left: '5%',
            right: '4%',
            bottom: '15%',
            containLabel: true
        },
        legend: { show: false },
        tooltip: {
            show: true,
            trigger: 'axis',
            backgroundColor: 'rgba(0,0,0,0.3)',
            borderColor: '#333', // 提示框浮层的边框颜色。
            borderWidth: 0, // 提示框浮层的边框宽。
            padding: 5, // 提示框浮层内边距，
            textStyle: { // 提示框浮层的文本样式。
                color: '#fff',
                fontStyle: 'normal',
                fontWeight: 'normal',
                fontFamily: 'sans-serif',
                fontSize: 12,
            },
            formatter: function(params) {
                //name:类目轴数据，value：数值轴数据, dataIndex：数据的索引
                return params[0].name + '<br><span style="display:inline-block;margin-right:10px;border-radius:6px;width:10px;height:10px;background-color:#5470c6"></span>' + tdata[params[0].dataIndex]

            },
            extraCssText: 'box-shadow: 0 0 3px rgba(0, 0, 0, 0.3);',

        },
        xAxis: [{
            type: 'category',
            show: false,
            data: list.area_cart.chart_data,

        }],
        yAxis: {
            type: 'value',
            axisLine: {
                show: false, //是否显示轴线
            },
            splitLine: {
                show: true,
                lineStyle: {
                    color: ['#2c4457'],
                    width: 1,
                    type: 'solid'
                }　　
            },
            axisLabel: {
                margin: 5,
                textStyle: {
                    fontSize: 12,
                },
                formatter: function(value) {
                    return Math.ceil(value * value * value / 10, 1) * 10
                }
            }


        },
        series: [{
                type: 'line',
                smooth: true,
                symbol: 'none',
                areaStyle: {
                    color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                            offset: 0,
                            color: 'rgba(76,178,193,0.5)'
                        },
                        {
                            offset: 1,
                            color: 'rgba(76,178,193,0.0)'
                        }
                    ])
                },
                itemStyle: {
                    normal: { lineStyle: { color: '#4cb2c1' } }
                }
            },

        ]
    };

    Chart1 = echarts.init(document.getElementById('chart_totle'));

    if (type == 'user') {
        // option.series[0].data = list.area_cart.ranch_total;
        var tdata = list.area_cart.ranch_total;
        // option.series[0].data = format_y_data(tdata);
        option.series[0].data = tdata.map(i => Math.cbrt(i))
        Chart1.clear();
        Chart1.setOption(option);
    } else if (type == 'cattle') {
        var tdata = list.area_cart.cattle_total;

        option.series[0].data = tdata.map(i => Math.cbrt(i))
        Chart1.clear();
        Chart1.setOption(option);
    }
}

function sel_chart_user(obj, type) {
    $(".tab_age a").each(function(i, elem) {
        $(elem).removeClass("active");
        if (!obj && i == 0) {
            $(elem).addClass("active");
        }
    });
    if (obj) {
        $(obj).addClass("active");
    }


    var colorList = ['#5470c6', '#91cc75', '#fac858', '#ee6666', '#73c0de'];
    option = {
        tooltip: {
            show: false,
        },
        grid: { //柱状图位置
            top: "0%",
            left: '-10px',
            right: '0%',
            bottom: '5%',
            containLabel: true
        },
        series: [{
            type: 'pie',
            radius: ['40%', '70%'],
            avoidLabelOverlap: false,
            avoidLabelOverlap: false,
            itemStyle: {
                borderRadius: 5,
                borderColor: '#1a232c',
                borderWidth: 2,
                normal: {
                    borderRadius: 5,
                    borderColor: '#1a232c',
                    borderWidth: 2,
                    color: function(params) {

                        return colorList[params.dataIndex]
                    }
                }
            },
            label: {
                show: false,
                position: 'center'
            },

            labelLine: {
                show: false
            },

        }]
    };

    Chart2 = echarts.init(document.getElementById('user_totle'));
    if (type == 'age') {
        $("#chart_user_list").html(" ")
        $.each(list.area_cart.chart_age, function(index, value) {
            $("#chart_user_list").prepend('<li><b style="color:' + colorList[index] + ';">·</b><span style="color:' + colorList[index] + ';">' + value.p + '</span>' + value.name + '</li>');
        });
        option.series[0].data = list.area_cart.chart_age;
        Chart2.clear();
        Chart2.setOption(option);


        //jsarr=list.area_cart.cattle_total
    } else if (type == 'plam') {
        $("#chart_user_list").html(" ")
        $.each(list.area_cart.chart_plam, function(index, value) {
            $("#chart_user_list").prepend('<li><b style="color:' + colorList[index] + ';">·</b><span style="color:' + colorList[index] + ';">' + value.p + '</span>' + value.name + '</li>');
        });
        option.series[0].data = list.area_cart.chart_plam;
        Chart2.clear();
        Chart2.setOption(option);
    }
}

function sel_chart_market(obj, type) {
    $(".tab_market a").each(function(i, elem) {
        $(elem).removeClass("active");
        if (!obj && i == 0) {
            $(elem).addClass("active");
        }
    });
    if (obj) {
        $(obj).addClass("active");
    }
    Chart3 = echarts.init(document.getElementById('markt_totle'));

    option = {
        grid: { //柱状图位置
            top: "10%",
            left: '5%',
            right: '4%',
            bottom: '15%',
            containLabel: true
        },
        tooltip: {
            show: true,
            trigger: 'axis',
            backgroundColor: 'rgba(0,0,0,0.3)',
            borderColor: '#333', // 提示框浮层的边框颜色。
            borderWidth: 0, // 提示框浮层的边框宽。
            padding: 5, // 提示框浮层内边距，
            textStyle: { // 提示框浮层的文本样式。
                color: '#fff',
                fontStyle: 'normal',
                fontWeight: 'normal',
                fontFamily: 'sans-serif',
                fontSize: 12,
            },
            extraCssText: 'box-shadow: 0 0 3px rgba(0, 0, 0, 0.3);'
        },
        legend: { show: false },
        xAxis: [{
            type: 'category',
            show: false,
            data: list.area_cart.chart_data,
        }],
        yAxis: {
            type: 'value',
            axisLine: {
                show: false, //是否显示轴线
            },
            min: 10000,
            splitLine: {
                show: true,
                lineStyle: {
                    color: ['#2c4457'],
                    width: 1,
                    type: 'solid'
                }　　
            }
        },
        series: [{
                type: 'line',
                smooth: true,
                symbol: 'none',
                areaStyle: {
                    color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                            offset: 0,
                            color: 'rgba(76,178,193,0.2)'
                        },
                        {
                            offset: 1,
                            color: 'rgba(76,178,193,0.0)'
                        }
                    ])
                },
                itemStyle: {
                    normal: { lineStyle: { color: '#4cb2c1' } }
                }
            },

        ]
    };

    if (type == 'c') {
        option.series[0].data = list.area_cart.markt_c_total;
        Chart3.clear();
        Chart3.setOption(option);
    } else if (type == 'd') {
        option.series[0].data = list.area_cart.markt_d_total;
        Chart3.clear();
        Chart3.setOption(option);
    }
}



function sel_breeding_totle() {
    option = {
        grid: { //柱状图位置
            top: "10%",
            left: '5%',
            right: '4%',
            bottom: '15%',
            containLabel: true
        },
        legend: { show: false },
        tooltip: {
            show: true,
            trigger: 'axis',
            backgroundColor: 'rgba(0,0,0,0.3)',
            borderColor: '#333', // 提示框浮层的边框颜色。
            borderWidth: 0, // 提示框浮层的边框宽。
            padding: 5, // 提示框浮层内边距，
            textStyle: { // 提示框浮层的文本样式。
                color: '#fff',
                fontStyle: 'normal',
                fontWeight: 'normal',
                fontFamily: 'sans-serif',
                fontSize: 12,
            },
            formatter: function(params) {
                //name:类目轴数据，value：数值轴数据, dataIndex：数据的索引
                return params[0].name + '<br><span style="display:inline-block;margin-right:10px;border-radius:6px;width:10px;height:10px;background-color:#5470c6"></span>' + tdata[params[0].dataIndex]
            },
            extraCssText: 'box-shadow: 0 0 3px rgba(0, 0, 0, 0.3);'
        },
        xAxis: [{
            type: 'category',
            show: false,
            data: list.area_cart.chart_data,

        }],
        yAxis: {
            type: 'value',
            axisLine: {
                show: false, //是否显示轴线
            },
            splitLine: {
                show: true,
                lineStyle: {
                    color: ['#2c4457'],
                    width: 1,
                    type: 'solid'
                }　　
            },
            axisLabel: {
                margin: 5,
                textStyle: {
                    fontSize: 12,
                },
                formatter: function(value) {
                    return Math.ceil(value * value * value / 10, 1) * 10
                }
            }

        },
        series: [{
                type: 'line',
                smooth: true,
                symbol: 'none',
                areaStyle: {
                    color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                            offset: 0,
                            color: 'rgba(76,178,193,0.5)'
                        },
                        {
                            offset: 1,
                            color: 'rgba(76,178,193,0.0)'
                        }
                    ])
                },
                itemStyle: {
                    normal: { lineStyle: { color: '#4cb2c1' } }
                }
            },

        ]
    };

    Chart3 = echarts.init(document.getElementById('chart_yz_totle'));
    var tdata = list.area_cart.yz_breeding_total;
    option.series[0].data = tdata.map(i => Math.cbrt(i))
        // option.series[0].data = list.area_cart.yz_breeding_total;
    Chart3.clear();
    Chart3.setOption(option);
}


function sel_born_totle() {
    option = {
        grid: { //柱状图位置
            top: "10%",
            left: '5%',
            right: '4%',
            bottom: '15%',
            containLabel: true
        },
        legend: { show: false },
        tooltip: {
            show: true,
            trigger: 'axis',
            backgroundColor: 'rgba(0,0,0,0.3)',
            borderColor: '#333', // 提示框浮层的边框颜色。
            borderWidth: 0, // 提示框浮层的边框宽。
            padding: 5, // 提示框浮层内边距，
            textStyle: { // 提示框浮层的文本样式。
                color: '#fff',
                fontStyle: 'normal',
                fontWeight: 'normal',
                fontFamily: 'sans-serif',
                fontSize: 12,
            },
            formatter: function(params) {
                //name:类目轴数据，value：数值轴数据, dataIndex：数据的索引
                return params[0].name + '<br><span style="display:inline-block;margin-right:10px;border-radius:6px;width:10px;height:10px;background-color:#5470c6"></span>' + tdata[params[0].dataIndex]
            },
            extraCssText: 'box-shadow: 0 0 3px rgba(0, 0, 0, 0.3);'
        },
        xAxis: [{
            type: 'category',
            show: false,
            data: list.area_cart.chart_data,

        }],
        yAxis: {
            type: 'value',
            axisLine: {
                show: false, //是否显示轴线
            },
            splitLine: {
                show: true,
                lineStyle: {
                    color: ['#2c4457'],
                    width: 1,
                    type: 'solid'
                }　　
            },
            axisLabel: {
                margin: 5,
                textStyle: {
                    fontSize: 12,
                },
                formatter: function(value) {
                    return Math.ceil(value * value * value / 10, 1) * 10
                }
            }
        },
        series: [{
                type: 'line',
                smooth: true,
                symbol: 'none',
                areaStyle: {
                    color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                            offset: 0,
                            color: 'rgba(76,178,193,0.5)'
                        },
                        {
                            offset: 1,
                            color: 'rgba(76,178,193,0.0)'
                        }
                    ])
                },
                itemStyle: {
                    normal: { lineStyle: { color: '#4cb2c1' } }
                }
            },

        ]
    };

    Chart3 = echarts.init(document.getElementById('chart_born_totle'));
    var tdata = list.area_cart.ranch_born_total;
    option.series[0].data = tdata.map(i => Math.cbrt(i))

    // option.series[0].data = list.area_cart.ranch_born_total;
    Chart3.clear();
    Chart3.setOption(option);
}


function sel_pz_totle(obj, type) {

    var colorList = ['#5470c6', '#91cc75', '#fac858', '#ee6666', '#73c0de'];
    option = {
        tooltip: {
            show: false,
        },
        grid: { //柱状图位置
            top: "0%",
            left: '5%',
            right: '5%',
            bottom: '15%',
            containLabel: true
        },
        series: [{
            type: 'pie',
            radius: ['40%', '70%'],
            avoidLabelOverlap: true,
            itemStyle: {
                borderRadius: 5,
                borderColor: '#1a232c',
                borderWidth: 2,
                normal: {
                    borderRadius: 5,
                    borderColor: '#1a232c',
                    borderWidth: 2,
                    color: function(params) {

                        return colorList[params.dataIndex]
                    }
                }
            },
            label: {
                show: false,
                position: 'center'
            },

            labelLine: {
                show: false
            },
        }]
    };

    Chart2 = echarts.init(document.getElementById('chart_yz_pz'));
    option.series[0].data = list.area_cart.yz_pz_total;
    Chart2.clear();
    Chart2.setOption(option);

    $("#chart_pz_list").html(" ")
    var total = list.area_num.cattle_totle_gn_num * 1 + list.area_num.cattle_totle_jcmu_num * 1

    $.each(list.area_cart.yz_pz_total, function(index, value) {
        $("#chart_pz_list").prepend('<li><b style="color:' + colorList[index] + ';">·</b><span style="color:' + colorList[index] + ';">' + (value.value * 1 / total * 100).toFixed(2) + '%</span>' + value.name + '</li>');
    });

}



function sel_cy_totle(obj, type) {
    option = {
        grid: { //柱状图位置
            top: "10%",
            left: '5%',
            right: '4%',
            bottom: '15%',
            containLabel: true
        },
        legend: { show: false },
        tooltip: {
            show: true,
            trigger: 'axis',
            backgroundColor: 'rgba(0,0,0,0.3)',
            borderColor: '#333', // 提示框浮层的边框颜色。
            borderWidth: 0, // 提示框浮层的边框宽。
            padding: 5, // 提示框浮层内边距，
            textStyle: { // 提示框浮层的文本样式。
                color: '#fff',
                fontStyle: 'normal',
                fontWeight: 'normal',
                fontFamily: 'sans-serif',
                fontSize: 12,
            },
            formatter: function(params) {
                //name:类目轴数据，value：数值轴数据, dataIndex：数据的索引
                return params[0].name + '<br><span style="display:inline-block;margin-right:10px;border-radius:6px;width:10px;height:10px;background-color:#5470c6"></span>' + tdata[params[0].dataIndex]
            },
            extraCssText: 'box-shadow: 0 0 3px rgba(0, 0, 0, 0.3);'
        },
        xAxis: [{
            type: 'category',
            show: false,
            data: list.area_cart.chart_data,

        }],
        yAxis: {
            type: 'value',
            axisLine: {
                show: false, //是否显示轴线
            },
            splitLine: {
                show: true,
                lineStyle: {
                    color: ['#2c4457'],
                    width: 1,
                    type: 'solid'
                }　　
            },
            axisLabel: {
                margin: 5,
                textStyle: {
                    fontSize: 12,
                },
                formatter: function(value) {
                    return Math.ceil(value * value * value / 10, 1) * 10
                }
            }
        },
        series: [{
                type: 'line',
                smooth: true,
                symbol: 'none',
                areaStyle: {
                    color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                            offset: 0,
                            color: 'rgba(76,178,193,0.5)'
                        },
                        {
                            offset: 1,
                            color: 'rgba(76,178,193,0.0)'
                        }
                    ])
                },
                itemStyle: {
                    normal: { lineStyle: { color: '#4cb2c1' } }
                }
            },

        ]
    };

    Chart1 = echarts.init(document.getElementById('chart_cy_totle'));
    var tdata = list.area_cart.ranch_total;
    option.series[0].data = tdata.map(i => Math.cbrt(i))
        // option.series[0].data = list.area_cart.ranch_total;
    Chart1.clear();
    Chart1.setOption(option);

}



function sel_gm_totle() {

    var colorList = ['#5470c6', '#91cc75', '#fac858', '#ee6666', '#73c0de'];
    option = {
        tooltip: {
            show: false,
        },
        grid: { //柱状图位置
            top: "0%",
            left: '5%',
            right: '5%',
            bottom: '15%',
            containLabel: true
        },
        series: [{
            type: 'pie',
            radius: ['40%', '70%'],
            avoidLabelOverlap: true,
            itemStyle: {
                borderRadius: 5,
                borderColor: '#1a232c',
                borderWidth: 2,
                normal: {
                    borderRadius: 5,
                    borderColor: '#1a232c',
                    borderWidth: 2,
                    color: function(params) {
                        return colorList[params.dataIndex]
                    }
                }
            },
            label: {
                show: false,
                position: 'center'
            },

            labelLine: {
                show: false
            },
        }]
    };

    Chart2 = echarts.init(document.getElementById('chart_gm_totle'));
    option.series[0].data = list.area_cart.scale_total;
    Chart2.clear();
    Chart2.setOption(option);
    $("#chart_gm_list").html(" ")
    $.each(list.area_cart.scale_total, function(index, value) {
        $("#chart_gm_list").prepend('<li><b style="color:' + colorList[index] + ';">·</b><span style="color:' + colorList[index] + ';">' + value.p + '</span>' + value.name + '</li>');
    });

}

function sel_top_totle() {
    $("#chart_yzmc_list").html(" ")
    $.each(list.area_cart.top20.data, function(index, value) {
        $("#chart_yzmc_list").prepend('<li><img src="' + value.avatar + '" />' + value.name + '<p>养殖地区:' + value.township_cn + ' | 养殖数量:' + value.totalcattle + '头<p></li>');
    });
}